package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringBootCmdLineDemoApplication {

	public static void main(String[] args) {
		try (ConfigurableApplicationContext ctx = SpringApplication.run(SpringBootCmdLineDemoApplication.class)) {
			SpringBootCmdLineDemoApplication app = ctx.getBean(SpringBootCmdLineDemoApplication.class);
			app.run(args);
		}
	}

	public void run(String[] args) {
		System.out.println("Hello, Spring Boot!!");
	}
}
